
Westbrook <- read.csv("../Data/Westbrook_2017.csv", stringsAsFactors=FALSE, row.names=NULL); 
Harden <- read.csv("../Data/Harden_2017.csv", stringsAsFactors=FALSE, row.names=NULL)

#Convert the did not play etc to NA for the PTS feature
Westbrook$PTS<-as.numeric(Westbrook$PTS)
Harden$PTS<-as.numeric(Harden$PTS)
Westbrook<-na.omit(Westbrook) #remove missing values
Harden <- na.omit(Harden)

par(mfrow=c(2,2)) #Make 2,2 plots
hist(Westbrook$PTS, col = "lightblue", freq=FALSE, main='Westbrook Points 2016-2017', xlab='Points', ylab='probability');
lines(density(Westbrook$PTS), col='red')
qqnorm(Westbrook$PTS, main='Westbrook Points QQ Plot');
qqline(Westbrook$PTS);

hist(Harden$PTS, col = "lightblue", freq=FALSE, main='Harden Points 2016-2017', xlab='Points', ylab='probability');
lines(density(Harden$PTS), col='red')
qqnorm(Harden$PTS, main='Harden Points QQ Plot');
qqline(Harden$PTS);