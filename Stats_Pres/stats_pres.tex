\documentclass[xcolor=dvipsnames, unknownkeysallowed]{beamer}
\usepackage{subfig}
\usepackage{tabularx}
\usepackage{pgf,tikz}
\usepackage{placeins}
\usepackage{listings}
\usepackage[english]{babel}
\usepackage{pgf}
\usepackage{wrapfig}
\usepackage{graphicx}
\usepackage{amsmath, amssymb, amsthm, xfrac, amsfonts, amsxtra}
\usepackage{srcltx}
%\usepackage{epsfig}
\usepackage{verbatim}
\usepackage{bm}% bold math
\usepackage[latin1]{inputenc}
\usepackage[english]{babel}
\usepackage{epstopdf}
\usepackage{xcolor}

\usetikzlibrary{patterns,arrows,decorations.pathreplacing}
\setbeamertemplate{theorems}[numbered]
\usetikzlibrary{arrows, shapes}



%Define your own shorthand commands
\newcommand{\RR}{\mathbb{R}}
\newcommand{\FS}{\sum_{n=-\infty}^{\infty}}
\newcommand*{\figuretitle}[1]{%
    {\centering%   <--------  will only affect the title because of the grouping (by the
    \textbf{#1}%              braces before \centering and behind \medskip). If you remove
    \par\medskip}%            these braces the whole body of a {figure} env will be centered.
}
 \newcommand{\HRule}{\rule{\linewidth}{0.5mm}} 
\newcommand{\ST}{\mathbb{S}^1}
\newcommand{\BigO}{\mathcal{O}}

\AtBeginSection[]{
\frame<beamer>{ 
\frametitle{Outline}   
\tableofcontents[currentsection,subsections] 
 }
  }


%Delare Math Operators
\DeclareMathOperator{\Tan}{tan}
\DeclareMathOperator{\Sin}{sin}
\DeclareMathOperator{\Cos}{cos}
\DeclareMathOperator{\Arcsin}{arcsin}

%Renew some Math Operators
\renewcommand{\Re}{\operatorname{Re}}
\renewcommand{\Im}{\operatorname{Im}}

%Make some block styles:
\usepackage{tcolorbox}
\tcbuselibrary{skins}

\newtcolorbox{Question}[0]{bicolor, colframe=red!80!white, colback=blue!20!white, colbacklower=blue!75!black, collower=white,title=Question}

\newtcolorbox{myBlock}[1]{bicolor, colframe=green!50!black, colback=black!20!white, colbacklower=blue!75!black, collower=white,title=#1}

\usepackage{biblatex}

\definecolor{olive}{rgb}{0.3, 0.4, .1}
\definecolor{fore}{RGB}{249,242,215}
\definecolor{back}{RGB}{51,51,51}
\definecolor{title}{RGB}{255,0,90}
\definecolor{dgreen}{rgb}{0.,0.6,0.}
\definecolor{gold}{rgb}{1.,0.84,0.}
\definecolor{JungleGreen}{cmyk}{0.99,0,0.52,0}
\definecolor{BlueGreen}{cmyk}{0.85,0,0.33,0}
\definecolor{RawSienna}{cmyk}{0,0.72,1,0.45}
\definecolor{Magenta}{cmyk}{0,1,0,0}



\usetheme{AnnArbor}
\usecolortheme{spruce}

\title[Applied Statistics]{Learning Statistics Through a Technological, Project and Inquiry Based Pedagogy}
\author{Kevin Hannay}
\institute{Schreiner University}


\begin{document}

% For every picture that defines or uses external nodes, you'll have to
% apply the 'remember picture' style. To avoid some typing, we'll apply
% the style to all pictures.
\tikzstyle{every picture}+=[remember picture]

% By default all math in TikZ nodes are set in inline mode. Change this to
% displaystyle so that we don't get small fractions.
\everymath{\displaystyle}



\begin{frame}
\titlepage
\end{frame}


\begin{frame}{Outline}
\tableofcontents
\end{frame}

\section{MATH 2330 Course Objectives}
\begin{frame}{MATH 2330: Learning Objectives:}
By the end of this course students will be able to:
\begin{enumerate}
\item Select and create appropriate graphical representations for qualitative and quantitative data;
\item Apply the basic concepts of probability to statistical inference;
\item Determine whether data is normally distributed and utilize appropriate parametric or non-parametric methods in decision making and hypothesis testing;
\item \textcolor{red}{Understand and identify statistical fallacies;}
\item Apply and interpret linear regression and correlation in various applications;
\item Understand and use analysis of variance and chi square ($\chi^2$) tests in various applications.
\end{enumerate}
\end{frame}

\begin{frame}{Example Discussions}
\begin{columns}[T]
\column{0.6 \textwidth} {
\begin{enumerate}
\item Generate a random sequence of numbers, it this truly random?
\item If Fred is a shy, introvert who enjoys reading which is more likely? He is a librarian or he is a construction worker?
\item Multiple hypothesis testing (p-hacking). 
\includegraphics[scale=0.15]{cancer.png}
\end{enumerate}
}
\column{0.4 \textwidth} {
\begin{center}
\includegraphics[scale=0.33]{pie_chart_cartoon.eps}
\end{center}
}
\end{columns}
\end{frame}


\section{Technology}
\begin{frame}{Technology in the Statistics Classroom}
\begin{enumerate}
\item Traditional approaches to statistics focus on by-hand calculations using statistical tables
\item Have to use less powerful approaches in many cases to allow for hand calculations
\item Often leads to using artificial (boring) data sets. 
\item Loss of class time focusing on the mechanics of calculations, rather than developing intuition
\item No statistician does calculations this way.....
\end{enumerate}
\end{frame}

\begin{frame}{Why use R?}
\begin{enumerate}
\item \textcolor{blue}{Free}, open-source software which is extremely popular in industry, sciences and data science.
\item Extremely powerful and extensible format--capable of handling real data sets. 
\item Huge collection of online resources for students.
\item RStudio provides a nice interface (training wheels) to the program. 
\item Knowledge of basic R could provide an employment advantage to students. 
\end{enumerate}
\begin{center}
\includegraphics[scale=0.5]{rstudio.jpeg}
\end{center}
\end{frame}

\begin{frame}{Challenges to using R}
\begin{columns}[T]
\column{0.6 \textwidth}{
\begin{enumerate}
\item Command line format can be intimidating for students versus point and click spreadsheet programs.
\item Steeper learning curve and challenges with reading in data sets. 
\item Batteries not always included. 
\end{enumerate}
}
\column{0.4 \textwidth} {
\begin{center}
\includegraphics[scale=0.5]{outliers.jpeg}
\end{center}
}
\end{columns}
\end{frame}

\begin{frame}{Custom R Package}
To help mitigate these challenges I have written a custom R Package for the class. 
\begin{itemize}
\item Allows for all the data sets needed for the class to be included in one place
\item Also includes applets, extra functions and course notes for the students
\item Can be updated throughout the semester to add features and data sets the students find.
\end{itemize}
\end{frame}

\begin{frame}{New Features of the Class this Semester}
\begin{itemize}
\item Intuition Building Applets
\item Instructional Videos
\item In-class exercises
\item Emphasis on Exploratory Data Analysis and question formulation
\end{itemize}
\end{frame}


\section{Course Design}
\begin{frame}{Grading Breakdown}
\begin{center}
\small
\begin{tabular}{ |c|c| } 
 \hline
\textbf{Component} & \textbf{Grade Percentage \%} \\  \hline 
Midterm 1 & 13.33 \\ \hline
Midterm 2 & 13.33 \\ \hline
Midterm 3 & 13.33 \\ \hline
In Class Exercises &10 \\ \hline
EDA Project & 10 \\ \hline
Final Project &20 \\ \hline
Final Exam & 20 \\  
\hline
\end{tabular}
\end{center}
\begin{itemize}
\item All exams are open everything and taken outside of class. 
\item The project accounts to \textcolor{red}{30\% of the final grade}.
\item Daily exercises are submitted at the end of every class. 
\end{itemize}
\end{frame}

\begin{frame}{Core Component: Course Project}
\begin{itemize}
\item Students \textcolor{blue}{choose} a data set to use for their project. Most choose one of the provided data sets.
\item They may work in pairs if they want on the project.
\item Midway through the semester they turn in an Exploratory Data Analysis (EDA) project which gives an overview of the data set and develops a set of research questions.
\item The final project is a scientific style report (Intro, Methods, Results, Conclusions, References) on the data set. 
\end{itemize}
\end{frame}

\begin{frame}{On Going Challenges}
\begin{itemize}
\item Course exists on a island currently. No follow-up courses, no alignment between numerous statistics classes taught across campus.  
\item Growing class sizes will limit the ability to implement new pedagogies. 
\item Technology challenges. 
\item Diverse student body in the class creates challenges with choices of statistical techniques to teach. 
\item Students without a laptop computer can be left at a disadvantage. 
\end{itemize}
\end{frame}









\end{document}


%END--------------------------------------------------------------------------------------------------------