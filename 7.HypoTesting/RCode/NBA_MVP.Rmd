---
title: 'NBA Statistical Analysis: MVP'
author: "Kevin Hannay"
date: "April 15, 2017"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(captioner)
library(knitr)
library(xtable)
figs <- captioner(prefix="Figure")
```
## Background
Analysis of sports performance provides a great opportunity to observe statistics in action. In the last twenty years teams in the National Basketball association have made increasing use of **analytics** to quantify player performance. This is just another term for statistical analysis. In this notebook we will examine the Most Valuable Player race during the 2017 NBA season. Coming into the last week of the season the two primary candidates for the prestigious award were James Harden of the Houston Rockets and Russell Westbrook of the Oklahoma City Thunder. Those of us who are Spurs fans, would have liked Kawhi Leonard to be included in the discussion but that wasn't in the cards.

Traditionally, the Most Valuable Player award in the NBA is awarded as a combination of team success and individual performance. The 2017 season provides an interesting case for the relative weights of these criteria as the Houston Rockets have more regular seasons wins, while Russell Westbrook has broken several records for individual statistical performance. 

We can use statistical analysis to determine if these differences in performance are a product of random effects are whether they truly represent differences in skill. 


## Reading in NBA Statistics
Instead of looking at just the summary statistics (averages) for the two candidates performances we may read-in their performance in each games of the season from an online database. Below I use the `read.csv` R function to read in statistical information for each game in the 82 game NBA season. Using the `names` command we can see the statistics which are recorded for each game in the season. 

```{r warning=FALSE, tidy=TRUE}
Westbrook <- read.csv("../Data/Westbrook_2017.csv", stringsAsFactors=FALSE, row.names=NULL); #read the file
Harden <- read.csv("../Data/Harden_2017.csv", stringsAsFactors=FALSE, row.names=NULL)
names(Westbrook)
```
This contains a ton of information on each players performance in each game. Initially, we will just focus on the points they scored in each game. To work with this data I first have to remove games for each player which they were injured or sat out of the game. 
```{r warning=FALSE}
#Convert the did not play etc to NA for the PTS feature
Westbrook$PTS<-as.numeric(Westbrook$PTS)
Harden$PTS<-as.numeric(Harden$PTS)

Westbrook<-na.omit(Westbrook) #remove missing values
Harden <- na.omit(Harden)
```

## A Statistical Version of Basketball Scoring
Now that we have read in the game logs for our two players we may take a look at the probability distribution of them scoring a given amount in any given game this season. These distributions of points scored don't look very Gaussian to me. But using a QQ plot we can compare these with a Gaussian/Normal distribution. 

```{r echo=TRUE}
par(mfrow=c(2,2)) #Make 2,2 plots
hist(Westbrook$PTS, col = "lightblue", freq=FALSE, main='Westbrook Points Scored 2016-2017', xlab='Points', ylab='probability');
lines(density(Westbrook$PTS), col='red')
qqnorm(Westbrook$PTS, main='Westbrook Points Normal QQ Plot');
qqline(Westbrook$PTS);

hist(Harden$PTS, col = "lightblue", freq=FALSE, main='Harden Points Scored 2016-2017', xlab='Points', ylab='probability');
lines(density(Harden$PTS), col='red')
qqnorm(Harden$PTS, main='Harden Points Normal QQ Plot');
qqline(Harden$PTS);
```

From the QQ plots we can see that the points scored in each game distributions are not quite Gaussian. In the middle of the distributions they are relatively close but the extremes are not well described by a Gaussian. In addition we may calculate the mean $\mu$ and standard deviation $\sigma$ for the two points scored distributions. 
```{r}
mean(Westbrook$PTS)
sd(Westbrook$PTS)
mean(Harden$PTS)
sd(Harden$PTS)
```

Looking at the points distributions we can see they don't look very different and each player has a substantial variation in their performance from game-to-game as captured by the standard deviation $\sigma$. 

*The question we can now ask is whether Russell Westbrook is really a better scorer than James Harden, or if his higher average may be attributed to random chance.*

## Simulated Seasons and Confidence Intervals

To answer this question we can think of alternate universes where we play the NBA season over many times (say 20,000 times) and compute the points per game (PPG) for each player in those simulated seasons. To create these alternate universes we assume that the points scored by each player in a game is purely random and is drawn from the distribution of points we plotted above. Specifically we draw samples of 82 from the point distributions shown above and compute the mean of the sample. Each time we draw from those distributions we will get a slightly different answer for the average.

$\bar{X}_H=\frac{1}{82} \sum_{j=1}^{82} x^H_j$, where $x^H_j$ is a random number drawn from the distribution $f_H(x)$ shown above.

$\bar{X}_W=\frac{1}{82} \sum_{j=1}^{82} x^W_j$, where $x^W_j$ is a random number drawn from the distribution $f_W(x)$ shown above.


```{r}
sample_size<-82;
seasons<- 20000;

West_avg<-1:seasons;
Harden_avg<-1:seasons;

for (i in 1:seasons) {
  West_avg[i]<-mean(sample(Westbrook$PTS, sample_size, replace=TRUE));
  Harden_avg[i]<-mean(sample(Harden$PTS, sample_size, replace=TRUE));
}
```
We collect the values of $\bar{X}_H$ and $\bar{X}_W$ for our 10,000 simulated seasons and may plot these values as a distribution. 
```{r}
par(mfrow=c(2,2)) #Make 2,2 plots
hist(West_avg, col = "lightblue", freq=FALSE, main='Westbrook Simulated Seasons', xlab='PPG', ylab='probability');
lines(density(West_avg), col='red')
qqnorm(West_avg, main='Westbrook PPG Normal QQ Plot');
qqline(West_avg);

hist(Harden_avg, col = "lightblue", freq=FALSE, main='Harden Simulated Seasons', xlab='PPG', ylab='probability');
lines(density(Harden_avg), col='red')
qqnorm(Harden_avg, main='Harden PPG Normal QQ Plot');
qqline(Harden_avg)
```

Notice that the distributions $f_{\bar{X}_H}$ and $f_{\bar{X}_W}$ do look normally distributed as can be seen by looking at the QQ Plots. This, of course, is the central limit theorem at work! The central limit theorem tells us that the distribution of $\bar{X}=\frac{1}{N} \sum_{j=1}^N X_j$ approaches a Normal distribution for (almost) any random variable $X_j$. 

In our case $N=82$ which is relatively large, so we can say that $\bar{X}_{H,W}$ are $\approx$ Normal.

In addition, we know that $\mathbb{E}[\bar{X}_{H,W}]=\mathbb{E}[X^{H,W}_j]=\mu$ and we have, $$\sigma_{\bar{X}_{H,W}}=\frac{\sigma_{H,W}}{\sqrt{N}}.$$ 

Given the distribution of the sample means (season averages) we may compute confidence intervals around our estimates. For the simulated seasons we can just look for values such that for 95% (the confidence level) of the simulated seasons the average falls within those bounds. This may be easily done by using the quantiles of the data. 
```{r}
quantile(West_avg,c(0.025,0.975))
quantile(Harden_avg, c(0.025,0.975))
```

Alternatively, we may also use that the sample distribution is approximately normal to build our confidence intervals. For a normal approximation of the sampling distribution we have that a 95% confidence interval centered at $\mu$ is given by $(\mu-1.96 \sigma_{\bar{X}},\mu-1.96 \sigma_{\bar{X}}$ where $$\sigma_{\bar{X}_{H,W}}=\frac{\sigma_{H,W}}{\sqrt{N}}$$ and $\sigma_{H,W}$ is the standard deviation the points scored by the players in each game. We may estimate this using the standard deviation of the players performance during the 2016-2017 season computed from the gamelogs ($\sigma_W$=`r sd(Westbrook$PTS)`, $\sigma_H$=`r sd(Harden$PTS)`). 

Thus we have that, 
$$\sigma_{\bar{X}_H}=\frac{\sigma_H}{\sqrt{N}}\approx \frac{`r sd(Harden$PTS)`}{\sqrt{`r length(Harden$PTS)`}}=`r sd(Harden$PTS)/sqrt(length(Harden$PTS))`$$
and
$$\sigma_{\bar{X}_W}=\frac{\sigma_W}{\sqrt{N}}\approx \frac{`r sd(Westbrook$PTS)`}{\sqrt{`r length(Westbrook$PTS)`}}=`r sd(Westbrook$PTS)/sqrt(length(Westbrook$PTS))`$$

Therefore, the normal approximation gives the following 95% confidence intervals for the season PPG averages for our two players:
James Harden
$$(`r mean(Harden$PTS)-1.96*sd(Harden$PTS)/sqrt(length(Harden$PTS))`, `r mean(Harden$PTS)+1.96*sd(Harden$PTS)/sqrt(length(Harden$PTS))`)$$ 
and Russell Westbrook,
$$(`r mean(Westbrook$PTS)-1.96*sd(Westbrook$PTS)/sqrt(length(Westbrook$PTS))`, `r mean(Westbrook$PTS)+1.96*sd(Westbrook$PTS)/sqrt(length(Westbrook$PTS))`)$$

Notice these estimates are nearly identical to the simulation based confidence intervals we found. This is because $\bar{X}_{H,W}$ are very close to being normally distributed. 

## Was Westbrook a better scorer during the 2016-2017 NBA Season?

We can see that under the 95% confidence intervals of Harden and Westbrook average PPG seasons overlap by a small amount using both the simulation and central limit theorem approach. 

We now ask our main question: Is their sufficient evidence from the 2017 season to conclude that Russell Westbrook was a better scorer (measured by his average points per game PPG). To evaluate this we need theory of hypothesis testing. For this we need a null hypothesis $H_0$ and an alternative hypothesis $H_a$:

* Null Hypothesis ($H_0$): We take the null hypothesis that $\mu_H=\mu_W$ that is the mean points per game between the two players is the same.
* Alternative Hypothesis ($H_a$): $\mu_H>\mu_W$ Harden actually averages more points than Westbrook. 

We will examine the data for evidence in support of the null hypothesis. 
To test this we may look at our simulated seasons and count up the number of times that James Harden averages more points than Russell Westbrook in those simulated seasons. Dividing this by the total number of simulated seasons gives an estimate for the odds that Westbrook averaged more points during the 2017-2016 season by luck alone.

```{r}
avg_diff<-West_avg-Harden_avg;
hist(avg_diff, freq=FALSE, main='Westbrook-Harden PPG in Simulated Seasons', xlab='PPG Difference', ylab='prob', breaks=50)
dens<-density(avg_diff)
lines(dens, col='red')
x1 <- min(dens$x);
x2 <- 0.0;
#with(dens, polygon(x=c(x[c(x1,x1:x2,x2)]), y= c(0,y[x1:x2],0), col="red"))
times_harden_better=length(avg_diff[avg_diff<0])/length(avg_diff)
```
Computing this we find that only about `r times_harden_better` percent of the simulated seasons show Harden averaging more points over the course of the season than Westbrook. The quantity we estimated from our simulations is known as a *p-value*. *P-values* measures the strength of the evidence against the null Hypothesis (small p-values indicate strong evidence against the null hypothesis). 

### T-test approach
We have learned a statistical test which might prove applicable to our question of whether sufficient evidence exists that Russell Westbrook is a superior scorer than James Harden. The test I am thinking of is the *t-test* for a difference in population means. To apply this test we need to assume that the points scored in each game distributions ($f_W$ and $f_H$) are approximately normal. 

```{r}
t.test(Westbrook$PTS, Harden$PTS, alternative='greater', paired=FALSE)
```

Notice that this approach gives a similar *p-value* to our simulation based approach to answering the question. This is in spite of the violation of the *T-test* assumption of normal population distributions. For large samples this assumption becomes less important to the efficacy of the *T-Test*. 



























