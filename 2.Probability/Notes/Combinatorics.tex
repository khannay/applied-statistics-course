\documentclass[11pt, a4paper]{article}
%\usepackage{geometry}
\usepackage[inner=2.5cm,outer=2.5cm,top=2.5cm,bottom=2.5cm]{geometry}
\pagestyle{empty}
\usepackage{graphicx}
\usepackage{fancyhdr, lastpage, bbding, pmboxdraw}
\usepackage[usenames,dvipsnames]{color}
\usepackage[colorlinks,pagebackref,pdfusetitle,urlcolor=darkblue,citecolor=darkblue,linkcolor=darkred,bookmarksnumbered,plainpages=false]{hyperref}
\usepackage{amsfonts}
\usepackage{listings}
\usepackage{caption}
\usepackage{placeins}
\DeclareCaptionFont{white}{\color{white}}
\DeclareCaptionFormat{listing}{\colorbox{gray}{\parbox{\textwidth}{#1#2#3}}}
\captionsetup[lstlisting]{format=listing,labelfont=white,textfont=white}
\usepackage{verbatim} % used to display code
\usepackage{fancyvrb}
\usepackage{acronym}
\usepackage{amsthm}
\usepackage{amsmath}
\usepackage{pgfplots}
%\usepackage[svgnames]{xcolor}
\VerbatimFootnotes


%Set up the headers
\pagestyle{fancyplain}
\fancyhf{}
\lhead{ \fancyplain{}{\classNum} }
\chead{ \fancyplain{}{\topic} }
\rhead{ \fancyplain{}{Dr. Hannay} }
%\rfoot{\fancyplain{}{page \thepage\ of \pageref{LastPage}}}
\fancyfoot[RO, LE] {page \thepage\ of \pageref{LastPage} }
\thispagestyle{plain}


%Set up the theorem enviroment
\newtheoremstyle{break}
  {\topsep}{\topsep}%
  {\itshape}{}%
  {\bfseries}{}%
  {\newline}{}%
\theoremstyle{break}

\newtheorem{theorem}{\underline{Theorem:}}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{conj}[theorem]{Conjecture}
\newtheorem{defn}[theorem]{\underline{Definition:}}

%Set up custom commands
\renewcommand\thesection{\Alph{section}}
%exercise command \ex{question}{space times 0.5 inches}
\newcommand{\ex}[2]     {
 #1 \newline 
\newcount\Scount
\Scount=0
\loop\vspace*{0.5in}\par\goodbreak\advance\Scount by 1 \ifnum\Scount< #2 \repeat
}%Makes an exercise with space left for the students to write the answer
\newcommand{\makesol}[1]     {
\newcount\Scount
\Scount=0
\loop\vspace*{0.5in}\par\goodbreak\advance\Scount by 1 \ifnum\Scount< #1 \repeat
}%Makes an exercise with space left for the students to write the answer

%Probability/Stats specific shorthands
\newcommand{\dlim}{\lim_{h\rightarrow0}}
\newcommand{\prob}[1]{{\mathbb{P}(#1)}}
\newcommand{\condprob}[2]{{\prob{#1|#2}=\frac{\prob{#1 \cap #2}}{\prob{#2}}}}

%Make ShortHands for quick updates
\renewcommand{\thefootnote}{\fnsymbol{footnote}}
\newcommand{\topic}{{Combinatorics}}
\newcommand{\classNum}{{MATH 2330}}

\DeclareSymbolFont{extraup}{U}{zavm}{m}{n}
\DeclareMathSymbol{\varheart}{\mathalpha}{extraup}{86}
\DeclareMathSymbol{\vardiamond}{\mathalpha}{extraup}{87}


\definecolor{OliveGreen}{cmyk}{0.64,0,0.95,0.40}
\definecolor{CadetBlue}{cmyk}{0.62,0.57,0.23,0}
\definecolor{lightlightgray}{gray}{0.93}
\definecolor{darkblue}{rgb}{0,0,.6}
\definecolor{darkred}{rgb}{.7,0,0}
\definecolor{darkgreen}{rgb}{0,.6,0}
\definecolor{red}{rgb}{.98,0,0}


\lstset{
language=R,                          % Code langugage
basicstyle=\small\ttfamily,
    stringstyle=\color{DarkGreen},
    otherkeywords={0,1,2,3,4,5,6,7,8,9},
    morekeywords={TRUE,FALSE},
    deletekeywords={data,frame,length,as,character},
    keywordstyle=\color{blue},
    commentstyle=\color{darkgreen},
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
\begin{center}
{\Large \textsc{\underline{MATH 2330: Combinatorics}}}
\vspace{0.25in}
\end{center}

\section{Basic Principle of Counting}
Combinatorics just means the mathematics of counting things. 
\begin{defn}[Basic Principle of Counting]
If two experiments are performed such that experiment 1 has $N_1$ possible outcomes and experiment 2 has $N_2$ possible outcomes then taken together we have $N_1 \times N_2$ possible outcomes. 
\end{defn}

\subsection{Exercises:}
Use the basic principle of counting to find the number of outcomes for the following random experiments:
\begin{enumerate}
\item \ex{Two rolls of a six-sided dice}{3}
\item \ex{Flipping a coin three times?}{3}
\item \ex{How many DNA sequences are their which are 10 base pairs long? Hint: DNA has found bases $A,T,C,G$ }{3}
\item \ex{For a 100 meter dash final with 5 runners how many ways can the runners finish?}{4}
\end{enumerate}

\section{Permutations}
In the last exercise above we had to figure out how many ways we could order the five runners. Our logic for solving this generalizes to how many ways we can order any $N$ distinct objects. 

\begin{theorem}[Permutations]
The number of ways to order $N$ distinct objects is given by: $$N \times N-1 \times N-2 \times ... \times 2 \times 1=N!$$
\end{theorem}
We can use R to calculate permutations $N!$ using the command:
\begin{lstlisting}
factorial(N) #calculate N!
\end{lstlisting}


\subsection{Exercises:}
\begin{enumerate}
\item \ex{If you bought ten cans of soup, each different types, and loaded them into the grocery bag randomly. What are the odds that when you pull them out of the grocery bag they come out in alphabetical order?}{4}
\item \ex{How many ways can the cards in a standard deck (52 cards) be ordered?}{4}
\item \ex{How many orders can 8 French nobles be executed in, if they are drawn randomly from a holding cell with 8 occupants?}{4}
\end{enumerate}

\noindent In some cases we may want to know how many ways we can order a subset of elements. For example, how many four digit pins are possible if we can't repeat the same digit more than once? Well, we have 10 choice for the first digit, 9 for the second, 8 for the third and 7 for the fourth. Therefore $N=10\times9\times8\times7$. Notice we can also write this as $N=10!/6!$

\begin{theorem}[Permutations of Subsets]
The number of ways to order $r$ distinct objects taken from a set of $n$ distinct objects is: $$P(n,r)=\frac{n!}{(n-r)!}.$$
\end{theorem}

\subsection{Exercise:}
\begin{enumerate}
\item \ex{How many orders can 8 French nobles be executed in, if they are drawn randomly from a holding cell with 18 occupants?}{4}
\end{enumerate}

\section{Combinations}
We now want to consider the problem of counting outcomes for the case where the order doesn't matter. For example, we may want to find the total number of five card poker hands. Perhaps our first instinct is to use the permutations formulas from the last section. We have 52 distinct objects and we want to know how many ways we can draw five of them. This gives: $$P(52,5)=52!/47!=52\times 51\times50\times49\times48=5,997,600.$$ However, their is a subtle error in our calculation. Using the permutation formula means that we care about the order the cards appear in! For example, the permutation way of counting means that the two hands:
\begin{align}
& 2 \varheart, 3 \spadesuit, 8 \clubsuit, 2 \spadesuit, \text{Ace} \vardiamond \nonumber \\
& \text{Ace} \vardiamond, 2 \varheart, 3 \spadesuit, 8 \clubsuit, 2 \spadesuit \nonumber
\end{align}
are being counted as separate hands! This isn't what we really want to count as these two hands are entirely equivalent in a game of poker. 

To correct this over counting, we just need to divide by the \textit{ number of equivalent entries} in our count. In this case we have $5!$ poker hands which are equilvalent. Therefore, we have the total number of five card poker hands is:
\begin{align}
P_5=\frac{52!}{5! 47!}=2,598,960 \nonumber
\end{align}
So just about 2.6 million unique five card hands. As usual we can generalize our poker analysis to hold for general collections of objects. 
\begin{theorem}[Combinations]
The number of ways to draw k distinct objects from a set of $N$ distinct objects  is given by: $$ C(N,k)=\binom{N}{k}=\frac{N!}{(N-k)!k!}$$
\end{theorem}
In R we can calculate $C(N,k)=\binom{N}{k}$ using the command:
\begin{lstlisting}
choose(N,k) #calculate C(N,k) 
\end{lstlisting}

\subsection{Exercises:}
\begin{enumerate}
\item \ex{How many ways can a jury of 12 people be chosen from a pool of 30 candidates?}{2}
\item \ex{A fire team of 4 junior Marines must be chosen from a platoon with junior 36 Marines for a patrol. Also a sergeant must be chosen to lead the fire team from a pool of 5 candidates. How many ways can the patrol be chosen with four juniors and one leader? }{2}
\end{enumerate}


\end{document}