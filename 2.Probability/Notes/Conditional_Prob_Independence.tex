\documentclass[11pt, a4paper]{article}
%\usepackage{geometry}
\usepackage[inner=2.5cm,outer=2.5cm,top=2.5cm,bottom=2.5cm]{geometry}
\pagestyle{empty}
\usepackage{graphicx}
\usepackage{fancyhdr, lastpage, bbding, pmboxdraw}
\usepackage[usenames,dvipsnames]{color}
\usepackage[colorlinks,pagebackref,pdfusetitle,urlcolor=darkblue,citecolor=darkblue,linkcolor=darkred,bookmarksnumbered,plainpages=false]{hyperref}
\usepackage{amsfonts}
\usepackage{listings}
\usepackage{caption}
\usepackage{placeins}
\DeclareCaptionFont{white}{\color{white}}
\DeclareCaptionFormat{listing}{\colorbox{gray}{\parbox{\textwidth}{#1#2#3}}}
\captionsetup[lstlisting]{format=listing,labelfont=white,textfont=white}
\usepackage{verbatim} % used to display code
\usepackage{fancyvrb}
\usepackage{acronym}
\usepackage{amsthm}
\usepackage{amsmath}
\usepackage{pgfplots}
\VerbatimFootnotes


%Set up the headers
\pagestyle{fancyplain}
\fancyhf{}
\lhead{ \fancyplain{}{\classNum} }
\chead{ \fancyplain{}{\topic} }
\rhead{ \fancyplain{}{Dr. Hannay} }
%\rfoot{\fancyplain{}{page \thepage\ of \pageref{LastPage}}}
\fancyfoot[RO, LE] {page \thepage\ of \pageref{LastPage} }
\thispagestyle{plain}


%Set up the theorem enviroment
\newtheoremstyle{break}
  {\topsep}{\topsep}%
  {\itshape}{}%
  {\bfseries}{}%
  {\newline}{}%
\theoremstyle{break}

\newtheorem{theorem}{\underline{Theorem:}}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{conj}[theorem]{Conjecture}
\newtheorem{defn}[theorem]{\underline{Definition:}}

%Set up custom commands
\renewcommand\thesection{\Alph{section}}
%exercise command \ex{question}{space times 0.5 inches}
\newcommand{\ex}[2]     {
 #1 \newline 
\newcount\Scount
\Scount=0
\loop\vspace*{0.5in}\par\goodbreak\advance\Scount by 1 \ifnum\Scount< #2 \repeat
}%Makes an exercise with space left for the students to write the answer
\newcommand{\makesol}[1]     {
\newcount\Scount
\Scount=0
\loop\vspace*{0.5in}\par\goodbreak\advance\Scount by 1 \ifnum\Scount< #1 \repeat
}%Makes an exercise with space left for the students to write the answer

%Probability/Stats specific shorthands
\newcommand{\dlim}{\lim_{h\rightarrow0}}
\newcommand{\prob}[1]{{\mathbb{P}(#1)}}
\newcommand{\condprob}[2]{{\prob{#1|#2}=\frac{\prob{#1 \cap #2}}{\prob{#2}}}}

%Make ShortHands for quick updates
\renewcommand{\thefootnote}{\fnsymbol{footnote}}
\newcommand{\topic}{{Conditional Probability}}
\newcommand{\classNum}{{MATH 2330}}


\definecolor{OliveGreen}{cmyk}{0.64,0,0.95,0.40}
\definecolor{CadetBlue}{cmyk}{0.62,0.57,0.23,0}
\definecolor{lightlightgray}{gray}{0.93}
\definecolor{darkblue}{rgb}{0,0,.6}
\definecolor{darkred}{rgb}{.7,0,0}
\definecolor{darkgreen}{rgb}{0,.6,0}
\definecolor{red}{rgb}{.98,0,0}


\lstset{
%language=bash,                          % Code langugage
basicstyle=\ttfamily,                   % Code font, Examples: \footnotesize, \ttfamily
keywordstyle=\color{OliveGreen},        % Keywords font ('*' = uppercase)
commentstyle=\color{gray},              % Comments font
numbers=left,                           % Line nums position
numberstyle=\tiny,                      % Line-numbers fonts
stepnumber=1,                           % Step between two line-numbers
numbersep=5pt,                          % How far are line-numbers from code
backgroundcolor=\color{lightlightgray}, % Choose background color
frame=none,                             % A frame around the code
tabsize=2,                              % Default tab size
captionpos=t,                           % Caption-position = bottom
breaklines=true,                        % Automatic line breaking?
breakatwhitespace=false,                % Automatic breaks only at whitespace?
showspaces=false,                       % Dont make spaces visible
showtabs=false,                         % Dont make tabls visible
columns=flexible,                       % Column format
morekeywords={__global__, __device__},  % CUDA specific keywords
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
\begin{center}
{\Large \textsc{MATH 2330: Conditional Probability}}
\vspace{0.5in}
\end{center}

\section{Introduction}
Conditional probabilities allow us to include additional information into our calculation of probabilities. 
\subsection{Example}
\begin{enumerate}
\item What is the probability of a psychic being able to guess my birthday? \\
Call the event that the psychic guesses my birthday correctly $B$, then $$\prob{B}=\frac{1}{365}=\frac{\text{Number of Correct Guesses}}{\text{Number of Total Possibilities}}.$$
\item What about if the psychic knows that I was born in December? \\
Well this information limits the number of choices down to the 31 days in December. Now we have that 
$$\prob{B|M}=\frac{1}{31}.$$
This is much better odds of guessing the correct day. The notation $\prob{B|M}$ should be read ``The probability of $B$ given $M$''. In the psychic problem B is the event of guessing my birthday correctly and $M$ is the event that the month I was born in is December. 
\end{enumerate}
In practical situations we deal with this type of situation, where we wish to calculate the odds of something we have partial knowledge of. 
\begin{enumerate}
\item What is the probability your spouse is cheating on you, given the new information that you found strange text messages on their phone?
\item What is the probability of someone living past the age of 70? What about if you are given the added information that they smoke?
\item What is the probability of that you have the flu, given that you have a slight fever?
\end{enumerate}

\section{Mathematical Definition}

\begin{defn}[Conditional Probability]
The conditional probability of some event $A$ given the information that $B$ has occurred is given by: $$\prob{A|B}=\frac{\prob{A \cap B}}{\prob{B}}$$
\end{defn}
We can get an idea for why this formula works by looking at a Venn diagram for conditional probability (Figure.~\ref{Fig:CondProb}). 
\begin{figure}
\includegraphics[scale=0.5]{P361_z1.png}
\caption{ Venn Diagram of a conditional probability calculation. The numbers indicate the number of outcomes in each category. Knowledge that $C$ has occurred limits the possibilities down to 27 possible outcomes, and means only $13=B\cap C$ of the outcomes in $B$ are possible. \label{Fig:CondProb}}
\end{figure}
\FloatBarrier
\subsection{Examples}
\begin{enumerate}
\item What is the probability of rolling a three with a dice, given that we know that an odd number was rolled? \\
Let $\mathcal{O}=\{1,3,5\}$ be the event an odd number is rolled and $\mathcal{T}=\{3\}$ the event a three is rolled. Then by our conditional probability formula we have
\begin{align}
\prob{\mathcal{T}|\mathcal{O}}=\frac{\prob{\mathcal{T} \cap \mathcal{O}}}{\prob{\mathcal{O}}}=\frac{|\{3\} \cap \{1,3,5\} |}{| \{1,3,5\}|}=\frac{1}{3}
\end{align}
Note that $\prob{\mathcal{O}|\mathcal{T}}=\frac{\prob{\mathcal{T} \cap \mathcal{O}}}{\prob{\mathcal{T}}}=\frac{1/6}{1/6}=1$.  This leads us to the important discovery:
\begin{theorem}
In general  for two events $A$ and $B$: $$\prob{A|B}\neq\prob{B|A}$$ that is probability of A given B is not the same as the probability of B given A. 
\end{theorem}
As another example of this non-equality, let $A$ be the event that someone is an professional basketball player and $B$ be the event that someone is taller than 5.5 feet. Then the probability of B given A,  $\prob{B|A}$,  is quite large because most professional basketball players are much taller than 5.5. That is if someone is a professional basketball player you would be very surprised if they were shorter than 5.5 feet tall. However, if we consider the probability that someone in a professional basketball player given that they are taller then 5.5ft, that is $\prob{A|B}$, we can see it is quite small. This is because there are very few professional basketball players and many people taller than 5.5ft. Therefore,  concluding every above average height person you meet is on the San Antonio Spurs is going to be wrong most of the time. The lesson here is that knowledge of $A$ can tell us much more about $B$ then vice versa. 

\item Say 5\% of a particular emergency room patient arrive with pain in their lower right abdomen and must be rushed to surgery, and 1\% of the people arriving have appendicitis and pain in their lower right abdomen. If a new patient comes with pain in their lower right abdomen what is the probability they have appendicitis? \\
Let $A$ be the event they have appendicitis, and $P$ be the event they have abdominal pain. Then we want to compute the probability $$\condprob{A}{P}=\frac{0.01}{0.05}=\frac{1}{5}=20\%$$
\end{enumerate}

\subsection{Exercises}
\begin{enumerate}
\item \ex{In 2016 the Cleveland Cavaliers had the following record in home and away games: \\
\begin{center}
\begin{tabular}{|c|c|c|}
\hline
& Home & Away \\ \hline
Wins & 31 & 20 \\ \hline
Losses & 10 & 21\\
 \hline
\end{tabular}
\end{center}
Using this table find the probability of the Cavaliers winning a game $\prob{W}$ in 2016. Then find the conditional probabilities $\prob{W|H}$, the probability of winning given the game is a home game, and $\prob{W|A}$ the probability of them winning a game given it is an away game. }{6}
\end{enumerate}


\section{Independence}
Now that we have a knowledge of conditional probability we can return to the question of independence of random events. 

\begin{defn}[Independence]
We say two events $A$ and $B$ are independent if and only if: $$\prob{A|B}=\prob{A}$$ or $$\prob{B|A}=\prob{B}.$$
\end{defn}
Practically this means that the probability of $A$ occurring is not changed whatsoever by the knowledge that $B$ has occured. For example, the probability of me being struck by lightning is is no way effected by the color of my hair. 

If two events are independent then using the definition of conditional probability gives:
\begin{align}
\prob{A}=\condprob{A}{B} \\
\prob{A\cap B}=\prob{A} \times \prob{B}
\end{align}
This is called the ``AND'' rule of calculating probabilities because if $A$ and $B$ are independent events then the probability of $A$ AND $B$ occurring $\prob{A \cap B}$ is the product of the probabilities.


\section{Multiplicative Rule}
\noindent If $A$ and $B$ are dependent (that is not independent) then we can use the multiplicative rule of probabilities to find $\prob{A \cap B}$. This is a more general AND rule. 
\begin{theorem} [Multiplicative rule of probabilities]
For two events $A$ and $B$ which may be dependent we have that $$\prob{A \cap B}=\prob{B|A} \prob{A}=\prob{A|B} \prob{B}$$
\end{theorem}
 
 \subsection{Examples:}
 \begin{enumerate}
 \item Suppose you have 6 white socks, 4 red socks and 2 blue socks in a drawer. What is the probability that the first two socks you draw out of the drawer in the dark are white socks? \\ \noindent Let $W_1$ be the event the first sock is white and $W_2$ the event the second sock is white. We want $$\prob{W_1 \cap W_2}=\prob{W_2|W_1} \prob{W_1}, $$ using the multiplicative rule for probabilties. Now, $\prob{W_1}=6/12=0.50$,  but $\prob{W_2|W_1}=5/11$ because after drawing the first sock out we keep it. Therefore, their are only 11 socks left in the drawer, 5 of which are white. Therefore, we have
 \begin{align}
 \prob{W_1 \cap W_2}=\prob{W_2|W_1} \prob{W_1}=6/12 \times 5/11= 0.227
 \end{align}
 Notice that $\prob{W_1 \cap W_2} \neq \prob{W_1} \times \prob{W_2}=1/4$, this tells us that drawing the socks are not independent events. This is because we keep the first sock in our hand before drawing the second. This is called sampling \textit{without replacement} and this can often break independence between events. 
 \end{enumerate}
 
 
 \section{Law of Total Probability:}
 The law of total probability allows us to use break down a complex probability calculation into a series of simpler calculations using conditional probabilities. To use the Law of Total Probability we need to form what is called a \underline{partition} of our sample space. 
 
 \begin{defn} [Partition]
 A partition of the sample space $\omega$ is a collection of disjoint events (no overlap) $B_1, B_2, ... B_N$ whose union is $\Omega$. Such a partition divides any set $A$ into $N$ disjoint pieces. 
 \end{defn}
 
 The Venn diagram of a partition is shown in Fig~\ref{Fig:Partition}. 
 
 \begin{figure}
 \centering
 \includegraphics[scale=0.33]{Partition_pic.png}
 \caption{Here we partition the sample space $\Omega$ into 6 disjoint components $B_1, B_2, .... B_6$ \label{Fig:Partition}}
 \end{figure}
 \FloatBarrier
 
 \begin{theorem}[Law of Total Probability]
 Suppose we have a sample space $\Omega$ which can be broken into $N$ subsets $B_k$ each of which are disjoint (no overlap) and the union of these subsets gives the entire sample space (i.e $\cup_{k=1}^N B_k=\Omega$). Then if we want to find the probability of an event $\prob{A}$ we have: $$\prob{A}=\prob{A|B_1}\prob{B_1}+\prob{A|B_2}\prob{B_2}+...+\prob{A|B_N}\prob{B_N}$$
 \end{theorem}
 
 The law of total probability tells us we can compute the probability of an event by breaking it into a series of disjoint slices then adding up the contributions of these slices to get the total back. This will come in useful when the slices have probabilities which are easy to find. 
 
 \section{Examples:}
 \begin{enumerate}
 \item Suppose we have a well shuffled deck of cards what is the probability that the second card drawn is an ace? \\
\\ \underline{Solution:} \\
 Let $A$ be the event that the first card is an Ace and $A^c$ the event that the first card is not an ace. These two events are a partition of our sample space as the first card is either an Ace or not an Ace and it cannot be both. Therefore, we can use the Law of Total Probability to find the probability the second card is an Ace, call this event $S$. We have that,
 \begin{align}
 & \prob{S}=\prob{S|A} \prob{A}+\prob{S|A^c}\prob{A^c}  \label{Ex:LTP1}
 \end{align}
 We can find each of these probabilities easily:
 \begin{align}
 &\prob{A}=\frac{4}{52}=\text{Probability the first card is an Ace} \nonumber \\
 &\prob{A^c}=\frac{48}{52}=\text{Probability the first card not an Ace}  \nonumber  \\
 &\prob{S|A}=\frac{3}{51}=\text{Probability the second card is an Ace, when the first was an Ace} \nonumber   \\
 &\prob{S|A^c}\frac{4}{51}=\text{Probability the second card is an Ace, when the first card isn't an Ace} \nonumber   
 \end{align}
 Now plugging these back into our law of total probability (Equation.~\ref{Ex:LTP1})
 \begin{align}
& \prob{S}=\frac{3}{51}\times \frac{4}{52} + \frac{4}{51}\times \frac{48}{52} =\frac{4}{52} \nonumber 
 \end{align}
 \item Lets say that Chick-Fil-B has two chicken supply providers: Old McDonald's Chicken Inc and Sick Chickens Inc. Old McDonalds provides 70\% of the chicken and Sick Chickens Inc provides the other 30\% of the chicken. Unsurprisingly, Sick Chickens Inc has a spotty record and 40\% of their chickens give people food poisoning. Old McDonalds has a much better record with only 5\% of their chickens making people sick. If you eat at Chick-Fil-B what are the chances you get sick?  
 \\ \\ \underline{Solution:} \\
 Let $S$ be the event the chicken makes you sick. Let $M$ be the event the chicken came from Old MacDonald's Chicken Inc and $SC$ be the event it came from Sick Chicken Inc. Since the chicken had to come from one of those two suppliers, and can't have come from both we have a partition of our sample space. Then the law of total probability tells us that:
 \begin{align}
 \prob{S}&=\prob{S|M}\prob{M}+\prob{S|SC}\prob{SC} \\
 &=0.05 \times 0.70 + 0.40 \times 0.30=0.155=15.5\%
 \end{align}
 
 \end{enumerate}
 
 \section{Exercises}
 \begin{enumerate}
 \item \ex{Fred has never had MATH 2330 and thinks its a good idea to join a underground game of Russian roulette. To make things more interesting he has two pistols to choose from. Each of the guns has six chambers (six places to insert a bullet), although you know that one of the guns has two bullets loaded and the other only has a single bullet. Assuming he has to choose between the guns randomly what is the probability you escape unscathed?}{8}
 \end{enumerate}
 
 



\end{document}