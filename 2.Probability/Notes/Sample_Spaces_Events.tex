\documentclass[11pt, a4paper]{article}
%\usepackage{geometry}
\usepackage[inner=2.5cm,outer=2.5cm,top=2.5cm,bottom=2.5cm]{geometry}
\pagestyle{empty}
\usepackage{graphicx}
\usepackage{fancyhdr, lastpage, bbding, pmboxdraw}
\usepackage[usenames,dvipsnames]{color}
\usepackage[colorlinks,pagebackref,pdfusetitle,urlcolor=darkblue,citecolor=darkblue,linkcolor=darkred,bookmarksnumbered,plainpages=false]{hyperref}
\usepackage{amsfonts}
\usepackage{listings}
\usepackage{caption}
\usepackage{placeins}
\DeclareCaptionFont{white}{\color{white}}
\DeclareCaptionFormat{listing}{\colorbox{gray}{\parbox{\textwidth}{#1#2#3}}}
\captionsetup[lstlisting]{format=listing,labelfont=white,textfont=white}
\usepackage{verbatim} % used to display code
\usepackage{fancyvrb}
\usepackage{acronym}
\usepackage{amsthm}
\usepackage{amsmath}
\usepackage{pgfplots}
\VerbatimFootnotes


%Set up the headers
\pagestyle{fancyplain}
\fancyhf{}
\lhead{ \fancyplain{}{\classNum} }
\chead{ \fancyplain{}{\topic} }
\rhead{ \fancyplain{}{Dr. Hannay} }
%\rfoot{\fancyplain{}{page \thepage\ of \pageref{LastPage}}}
\fancyfoot[RO, LE] {page \thepage\ of \pageref{LastPage} }
\thispagestyle{plain}


%Set up the theorem enviroment
\newtheoremstyle{break}
  {\topsep}{\topsep}%
  {\itshape}{}%
  {\bfseries}{}%
  {\newline}{}%
\theoremstyle{break}

\newtheorem{theorem}{\underline{Theorem:}}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{conj}[theorem]{Conjecture}
\newtheorem{defn}[theorem]{\underline{Definition:}}

%Set up custom commands
\renewcommand\thesection{\Alph{section}}
%exercise command \ex{question}{space times 0.5 inches}
\newcommand{\ex}[2]     {
 #1 \newline 
\newcount\Scount
\Scount=0
\loop\vspace*{0.5in}\par\goodbreak\advance\Scount by 1 \ifnum\Scount< #2 \repeat
}%Makes an exercise with space left for the students to write the answer
\newcommand{\makesol}[1]     {
\newcount\Scount
\Scount=0
\loop\vspace*{0.5in}\par\goodbreak\advance\Scount by 1 \ifnum\Scount< #1 \repeat
}%Makes an exercise with space left for the students to write the answer

%Probability/Stats specific shorthands
\newcommand{\dlim}{\lim_{h\rightarrow0}}
\newcommand{\prob}[1]{{\mathbb{P}(#1)}}
\newcommand{\condprob}[2]{{\prob{#1|#2}=\frac{\prob{#1 \cap #2}}{\prob{#2}}}}

%Make ShortHands for quick updates
\renewcommand{\thefootnote}{\fnsymbol{footnote}}
\newcommand{\topic}{{Sample Spaces and Events}}
\newcommand{\classNum}{{MATH 2330}}


\definecolor{OliveGreen}{cmyk}{0.64,0,0.95,0.40}
\definecolor{CadetBlue}{cmyk}{0.62,0.57,0.23,0}
\definecolor{lightlightgray}{gray}{0.93}
\definecolor{darkblue}{rgb}{0,0,.6}
\definecolor{darkred}{rgb}{.7,0,0}
\definecolor{darkgreen}{rgb}{0,.6,0}
\definecolor{red}{rgb}{.98,0,0}


\lstset{
%language=bash,                          % Code langugage
basicstyle=\ttfamily,                   % Code font, Examples: \footnotesize, \ttfamily
keywordstyle=\color{OliveGreen},        % Keywords font ('*' = uppercase)
commentstyle=\color{gray},              % Comments font
numbers=left,                           % Line nums position
numberstyle=\tiny,                      % Line-numbers fonts
stepnumber=1,                           % Step between two line-numbers
numbersep=5pt,                          % How far are line-numbers from code
backgroundcolor=\color{lightlightgray}, % Choose background color
frame=none,                             % A frame around the code
tabsize=2,                              % Default tab size
captionpos=t,                           % Caption-position = bottom
breaklines=true,                        % Automatic line breaking?
breakatwhitespace=false,                % Automatic breaks only at whitespace?
showspaces=false,                       % Dont make spaces visible
showtabs=false,                         % Dont make tabls visible
columns=flexible,                       % Column format
morekeywords={__global__, __device__},  % CUDA specific keywords
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
\begin{center}
{\Large \textsc{\underline{MATH 2330: Samples Spaces and Events}}}
\vspace{0.25in}
\end{center}

\section{Introduction}
Now is the time to take a slight detour from the study of statistics into the sister field of probability theory. Probability theory provides much of the theoretical backbone for the study of statistics. The origins of probability theory come from gambling. In particular, the first person to apply the analysis given here was Geralamo Cardano and Italian gambler in the 1500's. Cardano was the first person to analyze the outcomes of ``games of chance'' in a structured and mathematical way. This gave him a tremendous advantage over his competitors in 1500's Italy. However, he was never able to fully capitalize on his advances as his family life was a 1500's version of the Jerry Springer show. 

\section{Sample Spaces}
Cardano's great idea was the concept of the \textbf{sample space} which will denote as $\Omega$. To determine the probability on a event occurring, Cardano's idea is to make a list of all the possible outcomes from a random event. For example, a random event might be flipping a coin in which case the set of possible outcomes (the sample space $\Omega$) is given by $\Omega=\{H,T\}$. If the random event is rolling a six-sided dice the sample space is $\Omega=\{1,2,3,4,5,6\}$. In blackjack the sample space is $\Omega=\{2,3,4,...21, B\}$, where the $B$ stands for busting. 

\subsection{Exercises:}
\begin{enumerate}
\item \ex{List out the possible outcomes from flipping a coin twice.}{2}
\item \ex{List out the possible outcomes for random event of choosing two finalists for an award with four applicants Ann, Jerry, Tom and Andy. }{2}
\end{enumerate}

\section{Law of Sample Spaces}
 
 For the special case where all events from a random event are equally likely we can use the \underline{Law of Sample Spaces} to calculate the probability of an event. 
 
 \begin{defn}[Law of Sample Spaces (LSS) ]
 If all the outcomes of a random event are equally likely then $$\prob{A}=\frac{|A|}{|\Omega|}=\text{ Probability of event A occurring. }$$ Where $|A|$ is the number of outcomes in $A$ and $|\Omega|$ is the total number of events in the sample space. 
 \end{defn}
 
 \ex{\underline{Graphically:}}{4}
 
 \subsection{Exercises:}
 Use the Law of Samples Spaces to Calculate the probabilities below:
 \begin{enumerate}
 \item \ex{Probability a fair coin flip is heads}{1}
 \item \ex{Probability a fair dice roll is greater than 3}{2}
 \item \ex{You are in a class with only four students. If you have to do a project with a partner what are the chances you are paired with your best friend. Assume the assignments are made entirely randomly.}{4}
 \item \ex{If you know that someones pin is 4 digits (0-9) long, what are your chances of guessing it?}{4}
 \item \ex{What are the odds that the sum of the rolls from two six-sided dice is 8?}{6}
 \end{enumerate}
 
\noindent We can already see that the LSS turns probability calculations into a problem of counting stuff. As the numbers get larger it quickly becomes untenable to count things by hand. For example, if we try and list out all possible ways outcomes from flipping a coin ten times this would take about 84 days working for 12 hours a day! (over 3 million possibilities). In order to make better use of the LSS we need better techniques to count things. 


\end{document}