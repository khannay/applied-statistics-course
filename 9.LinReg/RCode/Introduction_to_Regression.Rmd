---
title: "Introduction to Linear Regression"
author: "Dr. Hannay"
date: "November 8, 2017"
output:
  prettydoc::html_pretty:
    highlight: github
    number_sections: yes
    theme: cayman
    toc: yes
  pdf_document:
    highlight: tango
    number_sections: yes
    toc: yes
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r echo=FALSE}
library(knitr)
library(HannayIntroStats)
opts_chunk$set(tidy.opts=list(width.cutoff=60),tidy=TRUE)
```
\newcommand{\var}{\mathrm{Var}}
\newcommand{\prob}{{\mathbb{P}}}

# Statistical Models
We are now ready to start studying **relationship** between variables (columns) in our data. To begin our study of this vast topic we will consider Dr. Distel's Turtle data set again. First lets read this data into `R`. 
```{r}
data("Distel_Turtle_Data")
turtle=Distel_Turtle_Data #short name for the data set
colnames(turtle)
```
Considering this data set we might what to see whether the tail length of the turtles is related to their mass. First we will need to filter the data to get rid of any turtles whose mass or tail length weren't measured. We also rescale the two variables so they have more convenient units. 
```{r}
turtle=subset(turtle, !is.na(turtle$Mass))
turtle<-subset(turtle, !is.na(turtle$Tail.Length))
turtle$Mass<-turtle$Mass/1000 #weight in kg
turtle$Tail.Length <- turtle$Tail.Length/10.0 #tail length in centimeters
```
Looks like we have `r dim(turtle)[1]` turtles left where we measured both their Mass and Tail Length. We can begin by making a scatter plot of the tail length versus the mass of the turtles.
```{r}
plot(turtle$Mass, turtle$Tail.Length, main='Turtle Tails versus Mass', xlab='Mass (kg)', ylab='Tail Length(cm)', col=turtle$Species, xlim=c(0,20), ylim=c(0,32))
```

This plot seems to show that larger turtles also have longer tails. However, this could just be a random effect since we only have 75 measurements of turtles. This leads to the following questions:

1. How can we tell is the effect of mass on tail length is real or just us seeing trends in random data?

2. If the effect is real, and how can we measure the *strength of this effect*?

3. How much information does knowledge of the mass give us about the tail length?

4. Given the mass of a turtle can we we make an accurate prediction for the *tail length*?

To answer these questions we need to build a **statistical model** for the length of turtle tails based on their masses. A simple model for the effect of mass on tail length is a *linear model*: $$T_{tail}=\beta T_{mass}+\alpha+\epsilon_i$$ This is called a linear model because it takes the form of $y=mx+b$. In this case $\alpha$ is the y-intercept and $\beta$ is the slope of the line. The $\epsilon_i$ is an assumed **noise** term which allows for random  errors in the measurements. We assume these errors have a normal distribution with mean zero and standard deviation $\sigma_r$. For our turtle model we call $T_{tail}$ the *response variable* (y axis) and $T_{tail}$ is the called the *explanatory variable* (x axis). 

To specify the model we will need to find the slope $\beta$ and y-intercept $\alpha$. 

## Exercises:

1. Does a unique line exist which goes through all of our data points? If you fit a line to the data points what are you attempting to minimize?

2. What do you expect the slope $\beta$ to be if the explanatory variable (mass) and the response variable (tail length) are completely unrelated?
```{r}
x<-rnorm(1000)
y<-rnorm(1000) #no relationship between x and y
plot(x,y, col='red', main='Unrelated variables', cex=0.7)
```

3. What is your intuition for sign (positive/negative) for $\beta$ in  the turtle data set?

# Fitting a Linear Model in R
It turns out that a very way to choose the *best* $\alpha$ and $\beta$ is to minimize the sum of square distance between the data points and the model predictions. Suppose, we have a model with $N$ data points $(x_1,y_1), (x_2,y_2), ... (x_N,y_N)$, then we can measure the **Cost** of the model for one data point $y_j$ by finding the distance (squared) between this data point and the predicted value $\hat{y_j}(x_j)=\alpha+\beta x_j$. Summing up all these errors or **residuals** gives us a measure of how well the model describes the data. 
\begin{equation}
\text{Cost}(\alpha, \beta)=\sum_{j=1}^N r_j^2=\sum_{j=1}^N [y_j-(\alpha+\beta x_j)]^2
\end{equation}

The below plot shows the residuals as green arrows for a guess of $\alpha=10$, $\beta=1.5$ for the turtle model. The total cost is also printed below for this parameter choice. 

```{r, echo=FALSE}
makeSimpleResidualPlot(10,1.5, turtle$Mass, turtle$Tail.Length, main='Turtle Residual Plots')
```

For now I want to show you how to use `R` to fit a linear model and view the results. Here is the command to build a linear model for our turtle data and view a summary of the results. 
```{r}
res.turtle<-lm(Tail.Length~Mass, data=turtle)
summary(res.turtle)
```
We will learn what *all* this output (stats poop) means later. Let's see what our residual plot looks like for these values:

```{r, echo=FALSE, warning=FALSE}
makeSimpleResidualPlot(15,1.05, turtle$Mass, turtle$Tail.Length, main='Turtle Residual Plots: Best Fit Parameters')
```

Notice that the cost (sum of all the residuals squared has decreased by quite a bit from our initial guess). This is the best (optimal) values of $\alpha$ and $\beta$ we could possibly choose. Any other choice of $\alpha, \beta$ would give a larger cost. 
Now we can look at the the estimates for the $\alpha$ and $\beta$ parameters that `R` finds:
```{r}
res.turtle$coefficients
```
The $\beta$ slope parameter is what is most important for our turtle model. The best point estimate for $\beta$ is $1.053$. In the context of the model this means that for every 1 kilogram increase in turtle weight we should expect their tail lengths to increase by about $1.05$ centimeters. We can see how well the value of $\beta$ is determined by the data by finding the confidence interval for $\beta$:
```{r}
confint(res.turtle, level=0.99)
```

We can also make a plot of the line that `R` fit to our turtle data. We can see that the line captures some of the big picture trends in the data. Bigger turtle usually (but not always) means a longer tail. 
```{r}
plot(Tail.Length~Mass, data=turtle, main='Turtle Tails versus Mass Linear Model', xlab='Mass (kg)', ylab='Tail Length (cm)', col=turtle$Species, xlim=c(0,20), ylim=c(0,32))
abline(res.turtle, col='blue')

```


## House Sales Price vs Square Footage
Lets consider a more interesting problem. In this section we will use linear regression to understand the relationship between the sales price of a house and the square footage of that house. Intuitively, we expect these two variables to be related, as bigger houses typically sell for more money. The data set comes from Ames, Iowa house sales from 2006-2010. First, lets read this data in and make a scatter plot of the sales price versus the square footage. 

```{r}
data("AmesHousing_Regression") #from HannayAppliedStats package
house<-AmesHousing_Regression #rename this data
house<-dropLowFactors(house, factor.column = 3, threshold = 30) #from HannayApplied Stats drop all neighborhoods with less than 30 data points
head(house)
```

We can see this has the log10 of the selling price, the square footage and the number of bathrooms in the house. 

```{r}
plot(house$Square.Feet, house$SalePrice, main='Real Estate Prices in Ames Iowa (Color by Neighborhood)', xlab='Square Footage (log10)', ylab='Sale Price ($) log10', col=house$Neighborhood, cex=0.5)
```

As expected we can see from the plot that square footage is somewhat important in determining the sales price of the house, but we can see that their is significant variation in the sales price for any given sqft size. Let's try and build a linear model for the relationship between the sqft of the houses and the sales price. 

```{r}
res.house<-lm(SalePrice.log10~Square.Feet.log10, data=house)
summary(res.house)
```

Lets look to see if the slope we found is significant (relative to a slope of zero):
```{r}
confint(res.house, level=0.99)
```
We can say that the slope is significantly greater than zero with a significance level $\alpha=0.01$ since this 99% confidence interval doesn't include zero. Finally, lets plot our regression line on the scatter plot:
```{r}
plot(house$Square.Feet, house$SalePrice, main='Real Estate Prices in Ames Iowa (Color by Neighborhood)', xlab='Square Footage (log10)', ylab='Sale Price ($) log10', col=house$Neighborhood, cex=0.5)
abline(res.house$coefficients)
```

Note, since we are dealing with the logarithms of the price and square footage here we these results tell us to *expect* a 1% increase in the square footage of the house to increase the Sales price by about 1% as well. In terms of the non logarithm transformed variables our model looks like $$Price=\alpha_0(Sqft)^{\beta}.$$ By taking the logarithm of both sides of this we get a linear equation $$\log(Price)=\log(\alpha_0)+\beta \log(Sqft)$$
```{r}
plot(10^house$Square.Feet, 10^house$SalePrice, main='Real Estate Prices in Ames Iowa (Color by Neighborhood)', xlab='Square Footage', ylab='Sale Price ($)', col=house$Neighborhood, cex=0.5)
x<-seq(0,5000,1)
alpha0<-10^2.35
y<-alpha0*x^0.90
lines(x,y, type='l')
```

## Exercise: Alligator Data Set
1. Lets get some practice fitting a linear model in R. Load the alligator data set, from the HannayAppliedStats package:

```{r, eval=FALSE}
library(HannayIntroStats)
data("alligator")
head(alligator)
```
Now fit a linear model to this data in `R` with the weight as the response variable and the snout vent length as the explanatory variable. 

+ What does the slope tell you for this model?
+ What is a 95% confidence interval for the slope?



# Assumptions of Linear Regression

Recall the form of our statistical model for linear regression is: 

$$ y_j=\beta_1 x_j+\alpha_0+\epsilon_j $$

1. **Linearity**: The most important assumption of linear regression is that the response variable $y$ is **linearly** dependent on the explanatory variable. This assumption forms the bedrock for the rest of our analysis, so when it is violated the entire model is invalid. The good news is that many relationships in nature are at least approximately linear. We can examine this assumption by looking at a scatter plot of the two variables, and by examining the residual plot. 

2. **Independence of Errors**: We assume that the errors added to our model (the $\epsilon_j$ terms) are all independent. 

3. **Equal Variance of Errors**: We assume the standard deviation of the errors is the same for all values of the explanatory variable $x_j$. Without this assumption we would need to perform what is called a weighted least squares on our data- which generally requires more data than a normal linear regression. This won't be covered in the class. The residual plot will reveal if this assumption is at least approximately valid.

4. **Normality of Errors**: The least important assumption is that the errors are normally distributed. If this is violated it doesn't have a effect on the best fit parameters, only in the estimation of the confidence intervals for those parameters. We can verify this assumption by making a QQ plot of the residuals. 




# Successful Linear Regression
In this notebook we will examine some metrics to test for how well our linear regression has performed for a set of data. 

To begin we make some fake data which fits the assumptions for linear regression analysis:
```{r}
beta0<-2.0;
beta1<-1.0;
x<-seq(0,10,0.05);
y<-beta0+beta1*x+rnorm(length(x),0.0,1.0); # random, independent normally distributed noise
plot(x,y)
```

We may know run a linear regression in $R$ using the $lm$ command,
```{r}
lm.results<-lm(y~x);
```
we store the results of the linear regression in the lm.results object. If we want a quick summary of the results we can use the `summary` command:
```{r}
summary(lm.results)
```
We may get confidence intervals on the parameters by running:
```{r}
confint(lm.results, level=0.95)
```

The following command is part of my package (HannayIntroStats) and makes a few plots automatically which are useful in determining whether linear regression is working on a data set.

```{r}
diagRegressionPlots(lm.results, x, y)
```

As expected since we created this fake data so that it satisfies each of the assumptions of regression it passes each of our tests. Starting with the histogram and QQ plot of the residuals. We can see from these two plots that the errors are approximately normally distributed (mound shaped histogram, and QQ plot roughly along the line). 

The top right plot shows the residual values as a function of the explanatory variable. We will see this plot will help us check for equal variance in the errors. In this case the width of the residuals is approximately the same as the x variable increases. This indicates the variance in the noise terms is constant. This plot also shows a flat tube of points centered around zero. If this is not the case then this indicates the first assumption (linearity) is violated. 

The bottom right plot shows the data plotted against the regression line model. 




# What Failure Looks Like

Now we will see what it looks like when the assumptions of linear regression are violated, and how we can tell from our diagnostic plots. These topics are roughly in the order of how serious these errors are. 

## Not a Linear Relationship between Variables

The most serious error occurs when we attempt to fit a linear regression line to data which clearly does not show a linear pattern. Many times this can be avoided by making a scatter plot of the data before you attempt to fit a regression line. For example, in the below plot we can see that their clearly is a nonlinear relationship between the variables x and y. 

```{r}
y<-beta0+beta1*sin(x)+rnorm(length(x),0.0,1.0);
plot(x,y)
```

Let's assume we ignores this and fit a linear model anyway.  

```{r}
lm.fail.notlinear<-lm(y~x)
summary(lm.fail.notlinear)
```

Now we can make some diagnostic plots for linear regression. 


```{r}
diagRegressionPlots(lm.fail.notlinear,x,y)
```

Notice that the residual plot in the top right shows a clear pattern. This is a sign that the relationship between the variables is nonlinear, and a linear model is not appropriate. 



## Errors are not independent

The next most important assumption for linear regression models is that the errors are independent. If this isn't the case then the errors can give false trends when we fit the model. 

```{r}
noise<-generateCorrelatedErrors(n=length(x), lag=5, sigma=2.0)
y<-beta0+beta1*x+noise
plot(x,y)
```

Lets make the linear model as usual. 
```{r}
lm.fail.notind<-lm(y~x)
summary(lm.fail.notind)
```

Now we can make some diagnostic plots for linear regression. 


```{r}
diagRegressionPlots(lm.fail.notind,x,y)
```

Notice that the residual plot has a weird shape/pattern to it. This is because the noise terms are not independent! These not independent random effects invalidate our linear model in this case. Typically, we can look for non-independence by looking for any non-random effects on the residual plot. 


## Unequal Variance in Residuals

The next assumption of linear regression analysis is that the variance (or standard deviation) of the error terms is constant across all values of the explanatory variable. This is easily checked by looking at the residual plot. If the variance is not constant then the residual plot rectangle will change widths as the explanatory (x) variable changes. 

```{r}
noise<-rnorm(length(x), sd=0.1)*(1.0+x)
y<-beta0+beta1*x+noise
lm.fail.var<-lm(y~x)
diagRegressionPlots(lm.fail.var,x,y)
```

In the above plot the residuals variance increases with x. This issue is correctable if we use weighted least squares analysis. 


## Non-Normality in Noise

This is not a huge concern for most linear regression models as they are not very sensitive to this assumption. However, our error terms need to be roughly mound shaped and continuous in nature to apply linear regression. If these are violated severely it will appear in the QQ plot and histogram of the residuals. 

For the example below I use a error (noise) term which is bi-modal. This is severe enough to invalidate the regression analysis. 

```{r}
noise<-rbimodalNormal(length(x),sigma1=0.25, sigma2=0.25)
y<-beta0+beta1*x+noise
lm.fail.normal<-lm(y~x)
diagRegressionPlots(lm.fail.normal,x,y)
```




# Goodness of Fit

The last topic we will discuss for linear regression are some measures of the **goodness of fits** for our models. These measurements are focused on how well the model performs in predicting the response variable in terms of the explanatory variable. 

## $R^2$ Coefficient of Determination and Measuring Model Fits

The residual standard error is given by $$\hat{\sigma}=\sqrt{\frac{\sum_{i=1}^N r_i^2}{N-k}}$$ where $N$ is the number of data points and $k$ the number of parameters estimated. This quantity gives us an idea about the raw accuracy of the regression model in its predictions. In other words the residual standard deviation is a measure of the average distance each observation falls from its prediction in the model. 

We can also describe the **fit** of a model using the $R^2$ value, which gives the fraction of the response variance explained by the statistical model. The unexplained variance is the variance of the residuals $\hat{\sigma}^2$, and let $s_y^2$ be the variance of the response variable data, then $$R^2=1-\frac{\hat{\sigma}^2}{s_y^2}$$ If the model tells us nothing about the relationship we expect to find $R^2=0$ meaning none of the variation in the response variable is explained by the model. On the other hand if the y values lie perfectly along a line we would have $\hat{\sigma}=0$ which gives $R^2=1$. In general the values of $R^2$ will lie somewhere between zero and one. 

One final note about the *goodness of fit* measures. They are often incorrectly used as a total measure of the utility of a model. While it is true that a linear model with a small $R^2$ value cannot precisely predict the response variable, these models can still tell us important things about our data (and life in general). As an example lets consider some (fake) data on the life expectancy of people given how many pounds of bacon they have consumed.

```{r}
data("bacon_data")
plot(bacon_data$bacon.lbs, bacon_data$life.expectancy)
lm.bacon<-lm(life.expectancy~bacon.lbs, data=bacon_data)
summary(lm.bacon)
confint(lm.bacon)
plot(bacon_data$bacon.lbs, bacon_data$life.expectancy)
abline(lm.bacon, col='blue')
```

In this analysis of the effect of eating bacon is on life expectancy, we don't expect for the amount of bacon to completely explain why one person lives longer than others. Therefore, we expect the bacon consumption will have a low $R^2$ value. Indeed we can see above that it does have a low value. However, we also find that for every pound of bacon we eat we expect to lose between 51 days and 21 days of life. According to our fake data bacon doesn't *predict* how long you are going to live, but it does have important effects on your life expectancy. 








#Homework

## Concept Questions:
Are the following statements True or False:

1. When conducting linear regression we assume that the error terms are independent

2. The $R^2$ term measures the goodness of fit for a linear model

3. Linear models with a small $R^2$ term should be discarded in all applications as they are poor predictive models. 

4. An assumption of linear regression analysis is that the error terms have equal variance.

5. The least squares solution finds the minimum of the sum of the residuals squared.

6. The standard error of the residuals gives a measure of the predictive power of a regression model.


## Practice Problems:
1. If we fit a linear model for the effect of a pesticide dosage (gallons sprayed) on the yield of tomatoes (pounds) on our field and find the best fit slope is $\beta=1.2$ and a 99\% confidence interval for the slope is given by $(0.75, 1.25)$ what can we say about the effect of spraying an additional gallon of pesticide on our field on our crop yield?
2. We have collected a data set on the amount of hours studying versus the grade on a final exam for 300 students. We find this plot has a slope a best fit slope of $5.0$ with a 95\% confidence interval of $(3.0,6.0)$. What can we conclude about the effects of studying for one additional hour? Do you expect this model to have a high $R^2$ value? 

## Advanced Problems:
For each of these data sets conduct a full regression analysis including an exploratory scatter plot, fit a linear model, form confidence intervals for the parameters and perform diagnostics to determine if the assumptions of linear regression are satisfied. 

1. Load the `alligator` data set and perform a linear regression analysis on the snout length versus the alligator weight.   
2. Load the `cricket_chirps` data set. Conduct a linear regression using the temperature as the explanatory variable and the chirps per second as the response variable. 
3. Load the `kidiq` data set. Conduct a linear regression analysis to see how well the mom's IQ (mom_iq) relates to the kid_score) column giving the child's IQ score.  What can you conclude about the genetic components of intelligence? 
4. Load the `NBA_Draft_Data` data set. Perform a linear regression analysis with the `Pick.Number` column as the explanatory variable and the `PTS` column as the response variable. Form a 99% confidence interval for the slope. What does this tell you about the value of an NBA draft pick? How many more points does the number one pick average than the 10th pick? 
















